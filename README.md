# Praktinė užduotis FCR Media

# About

This task is meant to evaluate candidates knowledge in php, js and css.
Task time frame is 6 working hours, must use git to track progress.

# Goal

- Using wordpress, php, css and js to create a Landing page.
- No addition pages are required, only the Landing page.
- Must use wordpress native api(For menus, content etc., hardcoding is not allowed).
- Must avoid additional plugins, anything over 10 in production is to much, in this case 5 is enough.
- Use git to track any changes to theme, so we could see the progress.
- Starter themes are allowed e.g _s, roots, wordpress starter theme.
- Css library should be used. (Bootstrap, bulma etc... choose)
- Code quality and structure is the most important part, so be smart about it.
- Design file is provided.
- Design must be responsive, but since you are using bootstrap you already know it, daah.
- Use photos from the internet, pixabay is fine, or place placeholders.
- Have fun in the process.

# Delivery

- Deliver only the theme folder or upload it to gitlab and send us the link.
